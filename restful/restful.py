import requests, yaml, urllib, time


class RestfulException(Exception):
    def __init__(self, message):
        message = 'RestfulException:\n{}'.format(message)
        super(RestfulException, self).__init__(message)


class Restful404Exception(Exception):
    def __init__(self):
        message = 'Restful404Exception'
        super(Restful404Exception, self).__init__(message)


class RestfulTimeoutException(Exception):
    def __init__(self):
        message = 'RestfulTimeoutException'
        super(RestfulTimeoutException, self).__init__(message)


class Restful:

    def __init__(self, url):
        self.url = url

    def one(self, field, id):
        return Member(self.url, field, id)

    def all(self, field):
        return Collection(self.url, field)

    def is_up(self):
        code = urllib.urlopen(self.url).getcode()
        return code == 200

    def wait_for_api(self, timeout=0):
        counter = 0
        ok = False
        while not ok:
            try:
                ok = self.is_up()
            except (Exception, IOError) as e:
                time.sleep(1)
                counter += 1
                if timeout != 0 and counter == timeout:
                    raise RestfulTimeoutException()


def handle_get_result(url, result):
    data = result.json()
    if 'error' in data:
        error = data['error']
        if error['status_code'] == 404:
            raise Restful404Exception()
        else:
            raise RestfulException(url + '\n' + yaml.safe_dump(data))
    return data


def handle_format_arguments(url, params):
    if params != {}:
        params_formatted = ''
        for key, value in params.iteritems():
            params_formatted += '{}={}&'.format(key, value)
        new_url = '{}?{}'.format(url, params_formatted)
    else:
        new_url = url

    return new_url


class Member:

    def __init__(self, url, field, id):
        self.url = '{}/{}/{}'.format(url, field, id)

    def get(self, params={}):
        url = handle_format_arguments(self.url, params)
        result = requests.get(url)
        data = handle_get_result(url, result)
        return data

    def delete(self):
        requests.delete(self.url)

    def one(self, field, id):
        return Member(self.url, field, id)

    def all(self, field):
        return Collection(self.url, field)


class Collection:

    def __init__(self, url, field):
        self.url = '{}/{}'.format(url, field)

    def get(self, params={}):
        url = handle_format_arguments(self.url, params)
        result = requests.get(url)
        data = handle_get_result(url, result)
        return data

    def post(self, data):
        result = requests.post(self.url, json=data)
        data = handle_get_result(self.url, result)
        return data
