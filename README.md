# restful-client

[![build status](https://gitlab.com/easymov/restful-client/badges/master/build.svg)](https://gitlab.com/easymov/restful-client/commits/master)
![coverage](https://gitlab.com/easymov/restful-client/badges/master/coverage.svg?job=coverage)
![PyPI version](https://badge.fury.io/py/restful-client.svg)

Restful is a simple REST client for Python.

## Install

```
pip install restful-client
```

## Quickstart

```
import restful
```
