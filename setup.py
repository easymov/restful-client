#!/usr/bin/env python

from setuptools import setup, find_packages

__version__ = '0.0.1'

setup(
    name='restful-client',
    version=__version__,
    description='REST client for Python',
    author='Easymov Robotics',
    author_email='dev@easymov.fr',
    url='https://gitlab.com/easymov/restful-client',
    install_requires=['future', 'pyyaml'],
    packages=find_packages(exclude=['tests*']),
)
